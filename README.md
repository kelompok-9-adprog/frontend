# AdmoDb Application

AdmoDb demo projects

---
### Guides for Setup Environment
1. Pull this repo first.
2. Setup gradle using `gradle version 4.5.x`.\
   **In Intellij IDEA** : <br/>
   `Project Structures` -> `Modules` -> "`+`" -> `Import module`
   and select the proper gradle version 4.5.x then click it.\
3. Then check if it's working in the terminal/cmd\
   *First terminal* : `gradle build --continuous`\
   *Second terminal* : `gradle bootRun`
   
   Notes: 
    - Spring boot doesn't have auto restart like Django when 
   something changes, so we need **two terminals** in order to use auto
   restart in Spring Boot a.k.a **Spring Dev Tools**.
   
   Quick overview:
   [https://www.baeldung.com/spring-boot-devtools]
   

#### Stacks/Tools (*will be added more*):
* [Tools Automation for Created Spring Boot Apps](https://start.spring.io/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [PostgreSQL for Spring Boot](https://www.callicoder.com/spring-boot-jpa-hibernate-postgresql-restful-crud-api-example/)
* [Spring Boot using Heroku](https://velmuruganv.wordpress.com/2017/01/15/gitlab-spring-boot-heroku-continuous-integration-and-deployment/)
---     

