package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProfileController {

    @GetMapping("/profile")
    public String profile(@RequestParam(name = "profile", required = false)
                                   String name, Model model) {
        model.addAttribute("profile", name);
        return "profile";
    }
}
