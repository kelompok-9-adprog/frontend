package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String homepage(@RequestParam(name = "home", required = true)
                                   String name, Model model) {
        model.addAttribute("home", name);
        return "index";
    }
}
