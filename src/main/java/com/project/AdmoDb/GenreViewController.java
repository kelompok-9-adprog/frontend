package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GenreViewController {

    @GetMapping("/moviegenre")
    public String genreList(){
        return "genre";
    }

    @GetMapping("/getmovie")
    public String movieList(@RequestParam(name = "genre") String genre, @RequestParam(name = "page") String page, Model model){
        return "movieResult";
    }
}
