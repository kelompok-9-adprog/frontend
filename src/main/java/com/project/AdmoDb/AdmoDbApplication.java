package com.project.AdmoDb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdmoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdmoDbApplication.class, args);
	}

}
