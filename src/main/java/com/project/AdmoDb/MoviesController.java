package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MoviesController {

    @GetMapping("/movies")
    public String movies(@RequestParam(name = "movies", required = false)
                                   String name, Model model) {
        model.addAttribute("movies", name);
        return "movies";
    }
}
