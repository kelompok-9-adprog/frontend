package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/signin")
    public String login(@RequestParam(name = "signin", required = false)
                                   String name, Model model) {
        model.addAttribute("signin", name);
        return "signin";
    }
}
