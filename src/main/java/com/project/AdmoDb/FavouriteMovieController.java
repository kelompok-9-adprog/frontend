package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FavouriteMovieController {

    @GetMapping("/favoritemovie")
    public String homepage(@RequestParam(name = "favoritemovie", required = false)
                                   String name, Model model) {
        model.addAttribute("favoritemovie", name);
        return "favoritemovie";
    }
}
