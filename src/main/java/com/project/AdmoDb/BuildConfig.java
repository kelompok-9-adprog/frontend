package com.project.AdmoDb;

import org.springframework.context.annotation.*;
import org.springframework.web.bind.annotation.*;

@Configuration
public class BuildConfig {
    public static final String API_KEY = "a5fc87dadba150d80b50f923b97fe181";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String LANGUAGE = "EN-US";
}