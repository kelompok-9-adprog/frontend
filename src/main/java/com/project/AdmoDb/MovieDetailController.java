package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MovieDetailController {

    @GetMapping("/detail")
    public String moviedetail(@RequestParam(name = "detail", required = false)
                                   String name, Model model) {
        model.addAttribute("detail", name);
        return "movie-details";
    }
}
