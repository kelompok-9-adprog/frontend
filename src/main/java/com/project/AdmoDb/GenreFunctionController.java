package com.project.AdmoDb;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import com.project.AdmoDb.BuildConfig;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RestController
public class GenreFunctionController {

    @RequestMapping(value = "/movielist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity<String> getMovie(@RequestParam(name = "genre") String genre,
                                           @RequestParam(name = "page") String page) {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.getForEntity(BuildConfig.BASE_URL + "discover/movie?with_genres="
                + genre + "&page=" + page + "&sort_by=popularity.desc&language=" + BuildConfig.LANGUAGE + "&api_key=" +
                BuildConfig.API_KEY, String.class);
        return response;
    }

    @RequestMapping(value = "/genrelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getGenreList() {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.getForEntity(BuildConfig.BASE_URL + "genre/movie/list?language="
                + BuildConfig.LANGUAGE + "&api_key=" + BuildConfig.API_KEY, String.class);
        return response;
    }
}
