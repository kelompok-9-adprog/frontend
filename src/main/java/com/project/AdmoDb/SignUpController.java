package com.project.AdmoDb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SignUpController {

    @GetMapping("/signup")
    public String login(@RequestParam(name = "signup", required = false)
                                String name, Model model) {
        model.addAttribute("signup", name);
        return "signup";
    }
}
