$(document).ready(function(){
    showGenres();

    function showGenres(){
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'genrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        $('<p>').append(
                        '<a href="/getmovie?genre=' + genre.id + '&page=1">' + genre.name + '</a>')
                        .appendTo('#genrelist');
                    });
                }
            });
        });
    }
});