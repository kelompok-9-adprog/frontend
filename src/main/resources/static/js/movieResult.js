var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();

$(document).ready(function(){

    fillGenreArray();
    getMovieResult();

    function getURLParameter(param){
        var pageURL = window.location.search.substring(1);
        var URLVariables = pageURL.split('&');
        for (var i = 0; i < URLVariables.length; i++)
        {
            var parameterName = URLVariables[i].split('=');
            if (parameterName[0] == param)
            {
                return parameterName[1];
            }
        }
    }

    function getMovieResult() {
        $(function() {
            var movieGenre = getURLParameter('genre');
            var resultPage = getURLParameter('page');
            $.ajax({
                type: 'GET',
                url: 'movielist?genre=' + movieGenre + '&page=' + resultPage,
                dataType: 'json',
                success: function(result){
                    $.each(result.results, function(i, movie) {
                        var thisGenre = getMovieGenreName(movie.genre_ids);
                        $('<tr>').append(
                            $('<td>').text(i+1),
                            $('<td>').append('<img src="http://image.tmdb.org/t/p/w185/' + movie.poster_path + '">'),
                            $('<td>').text(movie.title),
                            $('<td>').text(movie.vote_average),
                            $('<td>').text(thisGenre),
                            $('<td>').text(movie.overview),
                        ).appendTo('#movielist');
                    });
                }
            });
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'genrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";
        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});