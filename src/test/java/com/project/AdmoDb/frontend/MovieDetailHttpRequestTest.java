package com.project.AdmoDb.frontend;

import com.project.AdmoDb.MovieDetailController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovieDetailController.class)
public class MovieDetailHttpRequestTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getMovieDetailPage() throws Exception {
        this.mockMvc.perform(get("/detail"))
                .andExpect(view().name("movie-details"));
    }


}
