package com.project.AdmoDb.frontend;

import com.project.AdmoDb.LoginController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = LoginController.class)
public class LoginHttpRequestTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getHomePage() throws Exception {
        this.mockMvc.perform(get("/signin"))
                .andExpect(view().name("signin"));
    }


}
