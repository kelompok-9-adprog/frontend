package com.project.AdmoDb.frontend;

import com.project.AdmoDb.FavouriteMovieController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FavouriteMovieController.class)
public class FavouriteMovieHttpRequestTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getFavouriteMoviePage() throws Exception {
        this.mockMvc.perform(get("/favoritemovie"))
                .andExpect(view().name("favoritemovie"));
    }


}
