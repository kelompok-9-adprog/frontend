package com.project.AdmoDb.frontend;

import com.project.AdmoDb.ChatController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ChatController.class)
public class ChatHttpRequestTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getChatPage() throws Exception {
        this.mockMvc.perform(get("/chat"))
                .andExpect(view().name("chat"));
    }


}
